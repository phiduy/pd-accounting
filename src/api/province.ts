import { request } from '../config/axios';

export const getLocations = (data: { level: number; parentCode: number | string }) => {
	const endpoint = `/location?level=${data.level}&parentCode=${data.parentCode}`;
	return request(endpoint, 'GET', null);
};

export const getLocationsWithCode = (data: string) => {
	const endpoint = `/location/child/code?code=${data}`;
	return request(endpoint, 'GET', null);
};

export const getLocationByAddress = (data: { city: number; district: number; ward: number }) => {
	const endpoint = `/location/address?city=${data.city}&district=${data.district}&ward=${data.ward}`;
	return request(endpoint, 'GET', null);
};
