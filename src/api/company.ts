import { request } from '../config/axios';
import { ICompany, ICompanyType } from '../modules/Company/model/ICompanyState';

export const fetchCompanyTypes = (data: { page: number; limit: number }) => {
	const endpoint = `/companyType?page=${data.page}&limit=${data.limit}`;
	return request(endpoint, 'GET', null);
};

export const createCompanyType = (data: ICompanyType) => {
	const endpoint = `/companyType`;
	return request(endpoint, 'POST', data);
};

export const updateCompanyType = (data: { _id: string; companyType: ICompanyType }) => {
	const endpoint = `/companyType/${data._id}`;
	return request(endpoint, 'POST', data.companyType);
};

export const deleteCompanyType = (data: { _id: string }) => {
	const endpoint = `/companyType/${data._id}`;
	return request(endpoint, 'DELETE', null);
};

export const createCompany = (data: ICompany) => {
	const endpoint = `/companies`;
	return request(endpoint, 'POST', data);
};

export const updateCompany = (data: { _id: string; company: ICompany }) => {
	const endpoint = `/company/${data._id}`;
	return request(endpoint, 'POST', data.company);
};

export const deleteCompany = (data: { _id: string }) => {
	const endpoint = `/company/${data._id}`;
	return request(endpoint, 'DELETE', null);
};
