import * as React from 'react';
import { Form, Button, Input } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import { blue } from '@ant-design/colors';

export const TextBold = ({ text }: { text: string }) => (
	<div
		style={{
			color: '#333',
			fontSize: '1rem',
			fontWeight: 600,
		}}
	>
		{text}
	</div>
);

interface ITextEditableProps {
	text: string;
	message: string;
	// tslint:disable-next-line: ban-types
	onChange: Function;
}

export const TextEditable = ({ text, onChange, message }: ITextEditableProps) => {
	const [value, setValue] = React.useState(text);
	const [edit, setEdit] = React.useState(false);
	const [formInstance] = Form.useForm();

	const onFinish = () => {
		if (formInstance.getFieldValue('value') !== '') {
			setEdit(false);
			setValue(formInstance.getFieldValue('value'));
			onChange(formInstance.getFieldValue('value'));
		}
		setEdit(false);
	};

	return (
		<React.Fragment>
			{edit ? (
				<Form form={formInstance} initialValues={{ value: text }}>
					<Form.Item
						name="value"
						rules={[
							{
								required: true,
								message,
							},
						]}
						children={
							<Input
								onBlur={() => {
									onFinish();
								}}
								onPressEnter={() => {
									onFinish();
								}}
							/>
						}
					/>
				</Form>
			) : (
				<div style={{ display: 'inline-block' }}>
					{value}
					<Button
						type="text"
						icon={<EditOutlined style={{ color: blue.primary }} />}
						onClick={() => setEdit(true)}
					/>
				</div>
			)}
		</React.Fragment>
	);
};
