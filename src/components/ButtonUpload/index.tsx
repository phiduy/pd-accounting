import React from 'react';
import { UploadOutlined } from '@ant-design/icons';
import classnames from 'classnames';
import './index.scss';

export const ButtonUpload = ({
	onAccepted,
	onError,
}: {
	onAccepted: (file: File) => void;
	onError: (error: string) => void;
}) => {
	const [previewImages] = React.useState<{
		url: string | null;
		file: File | null;
	}>({
		url: null,
		file: null,
	});

	const [showLightBox, setShowLightBox] = React.useState(false);
	// const imageHandler = () => {
	//   const input = document.createElement('input');
	//   input.setAttribute('type', 'file');
	//   input.setAttribute('accept', 'image/*');
	//   input.click();
	//   input.onchange = () => {
	//     const files = input.files as FileList;
	//     const file = files[0] as File;
	//     // file type is only image.
	//     if (/^image\//.test(file.type)) {
	//       const url = URL.createObjectURL(file);
	//       onAccepted(file);
	//       setPreviewImages({
	//         file,
	//         url,
	//       });
	//     } else {
	//       onError('Upload file fail!!');
	//     }
	//   };
	// };
	return (
		<div className="upload_container">
			{/* <Button onClick={imageHandler}>
				<UploadOutlined /> Upload
			</Button> */}
			<div
				className={classnames({
					light_box: showLightBox,
					upload_preview: previewImages.file,
				})}
				onClick={() => {
					if (previewImages.file) {
						setShowLightBox(!showLightBox);
					}
				}}
			>
				{previewImages.file ? (
					<img src={previewImages.url as string} alt="previewImage" />
				) : (
					<div className="d-flex flex-column justify-content-center algin-items-center text-center">
						<UploadOutlined style={{ fontSize: 36, color: '#40a9ff' }} />
						<span className="text-center mb-1 mt-3">Chọn hình ảnh upload</span>
						<p className="text-muted">Mặt trước của chứng minh</p>
					</div>
				)}
			</div>
		</div>
	);
};
