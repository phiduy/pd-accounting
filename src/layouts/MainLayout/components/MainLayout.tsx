import * as React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { RouteConfig } from '../../../routes';
import { HeaderBar } from './Header';
import { IMainLayoutProps } from '../model/IMainLayoutProps';
import { NavigationBar } from './Navigation';
import { Transition } from 'react-transition-group';

import './MainLayout.scss';

interface IProps extends IMainLayoutProps {
	routes: RouteConfig[];
}
export class MainLayout extends React.Component<IProps> {
	render() {
		const { routes } = this.props;

		return (
			<React.Fragment>
				<div className="layout_wrapper">
					<HeaderBar />
					<div className="content_wrapper">
						<NavigationBar {...this.props} />
						<div className="content_body">
							<div className="content">
								<Switch>
									{routes.map((item) => {
										return (
											<Route
												key={item.path}
												path={item.path}
												component={item.component}
												children={() => (
													<Transition timeout={1000} in={true} appear={true}>
														{(status) => (
															<div className={`fade-in fade-in-${status}`}>
																<item.component {...this.props} />
															</div>
														)}
													</Transition>
												)}
											/>
										);
									})}
									{this.props.routes.length > 0 ? (
										<Redirect to={this.props.routes[0].path} />
									) : null}
									<Redirect from="*" to="/404" />
								</Switch>
								{this.props.children}
							</div>
						</div>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

export default MainLayout;
