import * as React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { ConfigProvider } from 'antd';
import IStore from './redux/store/IStore';
import { validateMessages } from './common/constants';
import { LoadingScreen } from './components';
import { mainRoutes, authRoutes } from './routes';
import { AuthLayout } from './layouts/index';
import MainLayout from './layouts/MainLayout/components/MainLayoutContainer';
import { ILogInState } from './modules/Login';
import locale from 'antd/es/locale/vi_VN';
import 'moment/locale/vi';

interface IProps {
	LoginState: ILogInState;
	router: any;
}
const App: React.FC<IProps> = (props) => {
	const { isLoading, accessToken } = props.LoginState;
	return (
		<React.Fragment>
			<ConfigProvider locale={locale} form={{ validateMessages }}>
				<Router>
					<React.Suspense fallback={<LoadingScreen size="large" />}>
						{isLoading ? (
							<LoadingScreen size="large" />
						) : accessToken !== null ? (
							<MainLayout routes={mainRoutes} />
						) : (
							<AuthLayout routes={authRoutes} />
						)}
					</React.Suspense>
				</Router>
			</ConfigProvider>
		</React.Fragment>
	);
};

const mapStateToProps = (store: IStore) => ({
	LoginState: store.LoginPage,
	router: store.router,
});
export default connect(mapStateToProps)(App);
