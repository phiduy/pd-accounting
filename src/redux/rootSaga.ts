/**
 * @file root sagas
 */

import { all } from 'redux-saga/effects';

// Place for sagas' app
import { sagas as LoginSaga } from '../modules/Login';
import { sagas as AccountNumberSaga } from '../modules/AccountNumber';
import { sagas as PermissionSaga } from '../modules/Permission';
import { sagas as CompanySaga } from '../modules/Company';
import { sagas as BankAccountSaga } from '../modules/BankAccount';
import { sagas as CompanyTypeSaga } from '../modules/CompanyType';
import { sagas as SystemConstantSaga } from '../modules/SystemConstant';
import { sagas as RoleSaga } from '../modules/Role';
import { sagas as CurrencySaga } from '../modules/Currency';
import { sagas as TaxSaga } from '../modules/Tax';
import { sagas as MainLayout } from '../layouts/MainLayout';

/*----Sagas List-----------------*/
export default function* rootSaga() {
	yield all([
		LoginSaga(),
		TaxSaga(),
		BankAccountSaga(),
		AccountNumberSaga(),
		PermissionSaga(),
		CompanySaga(),
		CompanyTypeSaga(),
		CurrencySaga(),
		SystemConstantSaga(),
		RoleSaga(),
		MainLayout(),
	]);
}
