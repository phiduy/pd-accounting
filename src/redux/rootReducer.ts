/**
 * @file Root reducers
 */

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { History } from 'history';
import IStore from './store/IStore';

// Place for reducers' app
import LoginPage, { name as nameOfLoginPage } from '../modules/Login';
import CompanyPage, { name as nameOfCompanyPage } from '../modules/Company';
import Tax, { name as nameOfTax } from '../modules/Tax';
import BankAccountPage, { name as nameOfBankAccountPage } from '../modules/BankAccount';
import PermissionPage, { name as nameOfPermissionPage } from '../modules/Permission';
import CurrencyPage, { name as nameOfCurrencyPage } from '../modules/Currency';
import CompanyTypePage, { name as nameOfCompanyTypePage } from '../modules/CompanyType';
import AccountNumberPage, { name as nameOfAccountNumberPage } from '../modules/AccountNumber';
import SystemConstantPage, { name as nameOfSystemConstantPage } from '../modules/SystemConstant';
import MainLayout, { name as nameOfMainLayout } from '../layouts/MainLayout';

/*----Reducers List-----------------*/
const reducers = {
	[nameOfTax]: Tax,
	[nameOfBankAccountPage]: BankAccountPage,
	[nameOfPermissionPage]: PermissionPage,
	[nameOfAccountNumberPage]: AccountNumberPage,
	[nameOfCurrencyPage]: CurrencyPage,
	[nameOfSystemConstantPage]: SystemConstantPage,
	[nameOfCompanyPage]: CompanyPage,
	[nameOfCompanyTypePage]: CompanyTypePage,
	[nameOfLoginPage]: LoginPage,
	[nameOfMainLayout]: MainLayout,
};

const rootReducer = (history: History) =>
	combineReducers<IStore>({
		...reducers,
		router: connectRouter(history),
	});

export default rootReducer;
