import { initialState as LoginInitialState, name as LoginPage } from '../../modules/Login';
import { initialState as CompanyInitialState, name as CompanyPage } from '../../modules/Company';
import { initialState as TaxInitialState, name as TaxPage } from '../../modules/Tax';
import {
	initialState as BankAccountInitialState,
	name as BankAccountPage,
} from '../../modules/BankAccount';
import {
	initialState as PermissionInitialState,
	name as PermissionPage,
} from '../../modules/Permission';
import {
	initialState as AccountNumberInitialState,
	name as AccountNumberPage,
} from '../../modules/AccountNumber';
import { initialState as CurrencyInitialState, name as CurrencyPage } from '../../modules/Currency';
import {
	initialState as SystemConstantInitialState,
	name as SystemConstantPage,
} from '../../modules/SystemConstant';
import {
	initialState as CompanyTypeInitialState,
	name as CompanyTypePage,
} from '../../modules/CompanyType';
import {
	initialState as MainLayoutInitialState,
	name as MainLayout,
} from '../../layouts/MainLayout';
import IStore from './IStore';

export const initialState: IStore = {
	[TaxPage]: TaxInitialState,
	[BankAccountPage]: BankAccountInitialState,
	[AccountNumberPage]: AccountNumberInitialState,
	[CurrencyPage]: CurrencyInitialState,
	[PermissionPage]: PermissionInitialState,
	[SystemConstantPage]: SystemConstantInitialState,
	[CompanyTypePage]: CompanyTypeInitialState,
	[CompanyPage]: CompanyInitialState,
	[LoginPage]: LoginInitialState,
	[MainLayout]: MainLayoutInitialState,
	router: null,
};

export default initialState;
