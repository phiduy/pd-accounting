import { ILogInState, name as LoginPageState } from './../../modules/Login';
import { ICompanyState, name as CompanyPageState } from './../../modules/Company';
import { ICurrencyState, name as CurrencyPageState } from './../../modules/Currency';
import { IPermissionState, name as PermissionPageState } from './../../modules/Permission';
import { ICompanyTypeState, name as CompanyTypePageState } from './../../modules/CompanyType';
import { IAccountNumberState, name as AccountNumberPageState } from './../../modules/AccountNumber';
import {
	ISystemConstantState,
	name as SystemConstantPageState,
} from './../../modules/SystemConstant';
import { IMainLayoutState, name as MainLayoutState } from '../../layouts/MainLayout';
import { IBankAccountState, name as BankAccountState } from '../../modules/BankAccount';
import { ITaxState, name as TaxState } from '../../modules/Tax';

export default interface IStore {
	[TaxState]: ITaxState;
	[BankAccountState]: IBankAccountState;
	[PermissionPageState]: IPermissionState;
	[AccountNumberPageState]: IAccountNumberState;
	[CurrencyPageState]: ICurrencyState;
	[CompanyTypePageState]: ICompanyTypeState;
	[CompanyPageState]: ICompanyState;
	[SystemConstantPageState]: ISystemConstantState;
	[LoginPageState]: ILogInState;
	[MainLayoutState]: IMainLayoutState;
	router: any;
}
