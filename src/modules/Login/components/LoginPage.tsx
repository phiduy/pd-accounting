import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { ILogInProps } from '../model/ILoginProps';
import { LoginForm } from './Form';
import { Row } from 'antd';

import './index.scss';

interface IProps extends RouteComponentProps, ILogInProps {}

export const LoginPage: React.FC<IProps> = (props) => {
	return (
		<Row justify="center" align="middle" className="sign_in_container">
			<div className="sign_in__content">
				<div className="text-center mb-5">
					<h5 className="mb-0 mt-3">Đăng nhập</h5>
					<p className="text-muted">
						bắt đầu cùng dịch vụ của <span className="text-danger">PD Accounting</span>
					</p>
				</div>
				<div className="auth-form">
					<LoginForm {...props} />
				</div>
			</div>
		</Row>
	);
};
