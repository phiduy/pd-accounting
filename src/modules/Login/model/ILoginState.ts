import { IPermission } from '../../../common/interfaces';

export interface IAdminInfo {
	username: string;
	password: string;
}

export interface ILogInState {
	accessToken: string | any;
	refreshToken: string | any;
	roleName: string | any;
	acceptedRole: string[] | any;
	rolePermissions: IPermission[];
	isProcessing: boolean;
	isLoading: boolean;
}

// InitialState
export const initialState: ILogInState = {
	roleName: localStorage.getItem('roleName') || null,
	rolePermissions: [],
	acceptedRole: localStorage.getItem('acceptedRole') || null,
	accessToken: localStorage.getItem('accessToken') || null,
	refreshToken: localStorage.getItem('refreshToken') || null,
	isLoading: false,
	isProcessing: false,
};
