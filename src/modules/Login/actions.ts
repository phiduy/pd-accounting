import { IError } from '../../common/interfaces';
import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { IAdminInfo } from './model/ILoginState';

//#region Admin Login Actions
export const adminLogin = (adminInfo: IAdminInfo): IActions.IAdminLogin => {
	return {
		type: Keys.ADMIN_LOGIN,
		payload: adminInfo,
	};
};

export const adminLoginSuccess = (res: any): IActions.IAdminLoginSuccess => {
	return {
		type: Keys.ADMIN_LOGIN_SUCCESS,
		payload: {
			data: res,
		},
	};
};

export const adminLoginFail = (res: IError[]): IActions.IAdminLoginFail => {
	return {
		type: Keys.ADMIN_LOGIN_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion
