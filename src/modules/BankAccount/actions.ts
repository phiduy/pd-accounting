import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { IBankAccount, BANK_ACCOUNT_MODAL, BANK_ACCOUNT_CLEAR } from './model/IBankAccountState';

export const toggleModal = (data: { type: BANK_ACCOUNT_MODAL }): IActions.IToggleModal => {
	return {
		type: Keys.TOGGLE_MODAL,
		payload: {
			...data,
		},
	};
};

export const handleClear = (type: BANK_ACCOUNT_CLEAR): IActions.IHandleClear => {
	return {
		type: Keys.HANDLE_CLEAR,
		payload: {
			type,
		},
	};
};

export const handleCurBankAccount = (data: {
	type: 'update' | 'delete';
	bankAccount: IBankAccount;
}): IActions.IHandleCurBankAccount => {
	return {
		type: Keys.HANDLE_CUR_BANK_ACCOUNT,
		payload: {
			...data,
		},
	};
};

//#region UPLOAD image Actions
export const uploadImage = (data: File): IActions.IUploadImage => {
	return {
		type: Keys.UPLOAD_IMAGE,
		payload: data,
	};
};
export const uploadImageSuccess = (res: { url: string }): IActions.IUploadImageSuccess => {
	return {
		type: Keys.UPLOAD_IMAGE_SUCCESS,
		payload: res,
	};
};
export const uploadImageFail = (res: any): IActions.IUploadImageFail => {
	return {
		type: Keys.UPLOAD_IMAGE_FAIL,
		payload: res,
	};
};
//#endregion

//#region GET Bank Account Actions
export const fetchBankAccount = (): IActions.IFetchBankAccount => {
	return {
		type: Keys.FETCH_BANK_ACCOUNT,
	};
};
export const fetchBankAccountSuccess = (res: any): IActions.IFetchBankAccountSuccess => {
	return {
		type: Keys.FETCH_BANK_ACCOUNT_SUCCESS,
		payload: res,
	};
};
export const fetchBankAccountFail = (res: any): IActions.IFetchBankAccountFail => {
	return {
		type: Keys.FETCH_BANK_ACCOUNT_FAIL,
		payload: res,
	};
};
//#endregion

//#region CREATE Bank Account Actions
export const createBankAccount = (data: IBankAccount): IActions.ICreateBankAccount => {
	return {
		type: Keys.CREATE_BANK_ACCOUNT,
		payload: data,
	};
};
export const createBankAccountSuccess = (res: any): IActions.ICreateBankAccountSuccess => {
	return {
		type: Keys.CREATE_BANK_ACCOUNT_SUCCESS,
		payload: res,
	};
};
export const createBankAccountFail = (res: any): IActions.ICreateBankAccountFail => {
	return {
		type: Keys.CREATE_BANK_ACCOUNT_FAIL,
		payload: res,
	};
};
//#endregion

//#region UPDATE Bank Account Actions
export const updateBankAccount = (data: {
	_id: string;
	bankAccount: {
		logo?: string;
		name?: string;
	};
}): IActions.IUpdateBankAccount => {
	return {
		type: Keys.UPDATE_BANK_ACCOUNT,
		payload: data,
	};
};
export const updateBankAccountSuccess = (res: any): IActions.IUpdateBankAccountSuccess => {
	return {
		type: Keys.UPDATE_BANK_ACCOUNT_SUCCESS,
		payload: res,
	};
};
export const updateBankAccountFail = (res: any): IActions.IUpdateBankAccountFail => {
	return {
		type: Keys.UPDATE_BANK_ACCOUNT_FAIL,
		payload: res.errors,
	};
};
//#endregion

//#region DELETE Bank Account Actions
export const deleteBankAccount = (data: { _id: string }): IActions.IDeleteBankAccount => {
	return {
		type: Keys.DELETE_BANK_ACCOUNT,
		payload: data,
	};
};
export const deleteBankAccountSuccess = (res: any): IActions.IDeleteBankAccountSuccess => {
	return {
		type: Keys.DELETE_BANK_ACCOUNT_SUCCESS,
		payload: res,
	};
};
export const deleteBankAccountFail = (res: any): IActions.IDeleteBankAccountFail => {
	return {
		type: Keys.DELETE_BANK_ACCOUNT_FAIL,
		payload: res.errors,
	};
};
//#endregion
