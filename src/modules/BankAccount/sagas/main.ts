import { call, put, takeEvery, delay } from 'redux-saga/effects';
import { message } from 'antd';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as BankAccountApi from '../../../api/bankAccount';
import * as FileApi from '../../../api/file';

// Handle GET Bank Accounts
function* handleFetchBankAccount(action: any) {
	try {
		const res = yield call(BankAccountApi.fetchAllBankAccount);
		yield delay(500);
		if (res.status === 200) {
			yield put(actions.fetchBankAccountSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.fetchBankAccountFail(error));
	}
}

// Handle CREATE Bank Account
function* handleCreateBankAccount(action: any) {
	try {
		const res = yield call(BankAccountApi.createBankAccount, action.payload);
		yield delay(500);
		if (res.status === 200) {
			message.success('Thêm tài khoản thành công', 0.5);
			yield put(actions.createBankAccountSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		message.error('Thêm tài khoản xảy ra lỗi', 0.5);
		yield put(actions.createBankAccountFail(error));
	}
}

// Handle UPDATE Bank Account
function* handleUpdateBankAccount(action: any) {
	try {
		const res = yield call(BankAccountApi.updateBankAccount, action.payload);
		yield delay(500);
		if (res.status === 200) {
			yield put(actions.updateBankAccountSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.updateBankAccountFail(error));
	}
}

// Handle DELETE Bank Account
function* handleDeleteBankAccount(action: any) {
	try {
		const res = yield call(BankAccountApi.deleteBankAccount, action.payload);
		if (res.status === 200) {
			message.success('Xóa ngân hàng thành công', 2);
			yield put(actions.deleteBankAccountSuccess(res.data.data));
			yield put(actions.fetchBankAccount());
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.deleteBankAccountFail(error));
	}
}

// Handle UPLOAD IMAGE
function* handleUploadImage(action: any) {
	try {
		const res = yield call(FileApi.uploadImage, action.payload);
		message.info('Đang upload hình ảnh', 2);
		if (res.status === 200) {
			yield put(actions.uploadImageSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.uploadImageFail(error));
	}
}
/*-----------------------------------------------------------------*/
function* watchFetchBankAccount() {
	yield takeEvery(Keys.FETCH_BANK_ACCOUNT, handleFetchBankAccount);
}
function* watchCreateBankAccount() {
	yield takeEvery(Keys.CREATE_BANK_ACCOUNT, handleCreateBankAccount);
}
function* watchUpdateBankAccount() {
	yield takeEvery(Keys.UPDATE_BANK_ACCOUNT, handleUpdateBankAccount);
}
function* watchDeleteBankAccount() {
	yield takeEvery(Keys.DELETE_BANK_ACCOUNT, handleDeleteBankAccount);
}
function* watchUploadImage() {
	yield takeEvery(Keys.UPLOAD_IMAGE, handleUploadImage);
}
/*-----------------------------------------------------------------*/
const sagas = [
	watchUploadImage,
	watchFetchBankAccount,
	watchCreateBankAccount,
	watchUpdateBankAccount,
	watchDeleteBankAccount,
];
export default sagas;
