/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IToggleModal
	| IActions.IHandleClear
	| IActions.IHandleCurBankAccount
	| IActions.IUploadImage
	| IActions.IUploadImageSuccess
	| IActions.IUploadImageFail
	| IActions.IFetchBankAccount
	| IActions.IFetchBankAccountSuccess
	| IActions.IFetchBankAccountFail
	| IActions.ICreateBankAccount
	| IActions.ICreateBankAccountSuccess
	| IActions.ICreateBankAccountFail
	| IActions.IUpdateBankAccount
	| IActions.IUpdateBankAccountSuccess
	| IActions.IUpdateBankAccountFail
	| IActions.IDeleteBankAccount
	| IActions.IDeleteBankAccountSuccess
	| IActions.IDeleteBankAccountFail;

export default ActionTypes;
