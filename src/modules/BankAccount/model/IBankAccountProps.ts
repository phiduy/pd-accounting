import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface IBankAccountProps {
	store: IStore;
	actions: typeof Actions;
}
