import * as React from 'react';
import { Form, Input, Button, Modal, Typography } from 'antd';
import { IBankAccountProps } from '../../model/IBankAccountProps';
import { BANK_ACCOUNT_MODAL, BANK_ACCOUNT_CLEAR } from '../../model/IBankAccountState';
import { UploadOutlined } from '@ant-design/icons';

const { Title } = Typography;

interface IProps extends IBankAccountProps {}

interface IInputs {
	name: string;
	logo: File | string;
}

export const CreateOrEditBankAccount = (props: IProps) => {
	const [formInstance] = Form.useForm();
	const {
		toggleModalAddEditBankAccount,
		curBankAccount,
		isProcessing,
		isUpdatingBankAccount,
		isUploadLogoSuccess,
		logoUrl,
	} = props.store.BankAccountPage;
	const [previewImages, setPreviewImages] = React.useState<any>();

	React.useEffect(() => {
		if (isUpdatingBankAccount && curBankAccount) {
			formInstance.setFieldsValue({
				name: curBankAccount.name,
				logo: curBankAccount.logo,
			});
		}
		if (isUploadLogoSuccess) {
			if (isUpdatingBankAccount && curBankAccount) {
				props.actions.updateBankAccount({
					_id: curBankAccount._id as string,
					bankAccount: {
						name: formInstance.getFieldValue('name'),
						logo: logoUrl,
					},
				});
			} else {
				props.actions.createBankAccount({
					name: formInstance.getFieldValue('name'),
					logo: logoUrl,
				});
			}
			props.actions.handleClear(BANK_ACCOUNT_CLEAR.UPLOAD_IMAGE);
		}
		// eslint-disable-next-line
	}, [isUpdatingBankAccount, formInstance, curBankAccount, isUploadLogoSuccess]);

	const imageHandler = () => {
		const input = document.createElement('input');
		input.setAttribute('type', 'file');
		input.setAttribute('accept', 'image/*');
		input.click();
		input.onchange = () => {
			const files = input.files as FileList;
			const file = files[0];
			// file type is only image.
			if (/^image\//.test(file.type)) {
				const url = URL.createObjectURL(file);
				// revokeObjectURL
				formInstance.setFieldsValue({ logo: file });
				setPreviewImages({
					file: file,
					url: url,
				});
			} else {
				console.log('fail');
			}
		};
	};

	const onFinish = (data: IInputs) => {
		if (isUpdatingBankAccount) {
			if (typeof formInstance.getFieldValue('logo') !== 'string') {
				props.actions.uploadImage(data.logo as File);
			} else {
				props.actions.updateBankAccount({
					_id: curBankAccount?._id as string,
					bankAccount: {
						name: data.name,
					},
				});
			}
		} else {
			props.actions.uploadImage(data.logo as File);
		}
	};

	return (
		<Modal
			visible={toggleModalAddEditBankAccount}
			onCancel={() => {
				formInstance.resetFields();
				props.actions.handleClear(BANK_ACCOUNT_CLEAR.BANK_ACCOUNT_UPDATE);
				props.actions.toggleModal({
					type: BANK_ACCOUNT_MODAL.ADD_EDIT_BANK,
				});
			}}
			afterClose={() => {
				formInstance.resetFields();
				setPreviewImages(undefined);
			}}
			maskClosable={false}
			footer={null}
		>
			<Title level={4}>Thông tin thẻ</Title>
			<Form form={formInstance} layout="vertical" onFinish={onFinish}>
				<Form.Item
					name="name"
					label="Ngân hàng"
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
					]}
					children={<Input allowClear={true} disabled={isProcessing} />}
				/>

				<Form.Item
					label="Logo"
					name="logo"
					rules={[
						{
							required: true,
						},
					]}
				>
					<Button disabled={isProcessing} onClick={() => imageHandler()}>
						<UploadOutlined /> Chọn hình ảnh
					</Button>
				</Form.Item>

				{previewImages && (
					<div style={{ width: 120 }}>
						<img alt="preview" style={{ maxWidth: '100%' }} src={previewImages.url} />
					</div>
				)}

				<Form.Item
					children={
						<div className="d-flex w-100 justify-content-end">
							<Button
								onClick={() =>
									props.actions.toggleModal({
										type: BANK_ACCOUNT_MODAL.ADD_EDIT_BANK,
									})
								}
								className="mr-2"
							>
								Đóng
							</Button>
							<Button type="primary" htmlType="submit" loading={isProcessing}>
								{isUpdatingBankAccount ? 'Cập nhật' : 'Thêm mới'}
							</Button>
						</div>
					}
				/>
			</Form>
		</Modal>
	);
};
