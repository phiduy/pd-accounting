import * as React from 'react';
import { Space, Table, Tooltip, Button, Popconfirm, Empty } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { IBankAccountProps } from '../../model/IBankAccountProps';
import { IBankAccount } from '../../model/IBankAccountState';
import { IColumn } from '../../../../common/interfaces';
import { useMediaQuery } from 'react-responsive';

const columns = (props: IBankAccountProps): IColumn[] => {
	return [
		{
			title: '#',
			dataIndex: 'key',
			key: 'key',
			width: 50,
		},
		{
			title: 'Ngân hàng',
			dataIndex: 'name',
		},
		{
			title: 'Logo',
			dataIndex: 'logo',
			key: 'logo',
			render: (text: any, record: IBankAccount) => (
				<>
					<img
						style={{ height: '40px' }}
						src={`${process.env.REACT_APP_BASE_URL}${record.logo}`}
						alt={record.name}
					/>
				</>
			),
		},
		{
			title: 'Hành động',
			dataIndex: 'operation',
			render: (text: any, record: IBankAccount) => (
				<Space>
					<Tooltip placement="top" title="Chỉnh sửa">
						<Button
							type="text"
							onClick={() =>
								props.actions.handleCurBankAccount({
									type: 'update',
									bankAccount: record,
								})
							}
							icon={<EditOutlined className="text-primary" />}
						/>
					</Tooltip>
					<Tooltip placement="top" title="Xóa">
						<Popconfirm
							title="Bạn có chắc chắn xoá ngân hàng này?"
							onConfirm={() =>
								props.actions.deleteBankAccount({
									_id: record._id as string,
								})
							}
							cancelText="Huỷ"
							okText="Xoá"
						>
							<DeleteOutlined className="text-danger" />
						</Popconfirm>
					</Tooltip>
				</Space>
			),
		},
	];
};

interface IProps extends IBankAccountProps {}

export const BankAccountTable: React.FC<IProps> = (props) => {
	const isTabletPortrait = useMediaQuery({ query: '(max-width: 767px)' });
	const { bankAccountRecords, isLoadingBankAccount, isProcessing } = props.store.BankAccountPage;

	return (
		<Table
			locale={{
				emptyText: <Empty description="Không có dữ liệu" />,
			}}
			rowKey="_id"
			scroll={{
				x: isTabletPortrait ? 1000 : 0,
			}}
			columns={columns(props)}
			loading={isLoadingBankAccount || isProcessing}
			dataSource={bankAccountRecords}
		/>
	);
};
