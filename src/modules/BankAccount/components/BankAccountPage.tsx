import * as React from 'react';
import { IBankAccountProps } from '../model/IBankAccountProps';
import { BANK_ACCOUNT_MODAL } from '../model/IBankAccountState';
import { RouteComponentProps } from 'react-router';
import { Button, Divider } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { CreateOrEditBankAccount } from './Form';
import { BankAccountTable } from './Table';
import { CustomBreadCrumb } from '../../../components';

interface IProps extends RouteComponentProps, IBankAccountProps {}

export const BankAccountPage: React.FC<IProps> = (props) => {
	React.useEffect(() => {
		props.actions.fetchBankAccount();
		// eslint-disable-next-line
	}, []);

	return (
		<React.Fragment>
			<CustomBreadCrumb title="Quản lý ngân hàng" path="/" />
			<div className="box">
				<div className="box_content">
					<Button
						type="primary"
						icon={<PlusOutlined />}
						onClick={() =>
							props.actions.toggleModal({
								type: BANK_ACCOUNT_MODAL.ADD_EDIT_BANK,
							})
						}
					>
						Thêm ngân hàng
					</Button>
					<Divider />
					<BankAccountTable {...props} />
				</div>
			</div>
			<CreateOrEditBankAccount {...props} />
		</React.Fragment>
	);
};
