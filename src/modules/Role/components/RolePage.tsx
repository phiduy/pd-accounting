import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { IRoleProps } from '../model/IRoleProps';
import { LoginForm } from './Form';
import { Row } from 'antd';

import './index.scss';

interface IProps extends RouteComponentProps, IRoleProps {}

export const RolePage: React.FC<IProps> = (props) => {
	return (
		<Row justify="center" align="middle" className="sign_in_container">
			<div className="sign_in__content">
				<div className="text-center mb-5">
					<h5 className="mb-0 mt-3">Đăng nhập</h5>
					<p className="text-muted">
						Trang quản trị <span className="text-danger">Cat English</span>
					</p>
				</div>
				<div className="auth-form">
					<LoginForm {...props} />
				</div>
			</div>
		</Row>
	);
};
