import React from 'react';
import { Row, Col, Input, Button, Typography, Form, Space, InputNumber, Divider } from 'antd';
import { IAccountNumberProps } from '../../model/IAccountNumberProps';
import { AccountNumberChildForm } from './AccountNumberChildForm';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { ACCOUNT_NUMBER_CLEAR, IAccountNumber } from '../../model/IAccountNumberState';
import { RouteComponentProps, useParams } from 'react-router-dom';

const { Title } = Typography;

interface IProps extends IAccountNumberProps, RouteComponentProps {}

interface IInputs {
	circulars: string;
	accountNumbers: {
		accountName: string;
		accountNumber: number;
		children?: IAccountNumber[];
	}[];
}

export const CreateOrEditAccountNumberForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const param: { circularsId: string } = useParams();
	const {
		isProcessing,
		currentAccountNumber,
		isUpdatingAccountNumber,
	} = props.store.AccountNumberPage;

	React.useEffect(() => {
		if (param.circularsId && currentAccountNumber) {
			formInstance.setFieldsValue({
				circulars: currentAccountNumber.circulars,
				accountNumbers: currentAccountNumber.accountNumbers,
			});
		}
		return () => {
			props.actions.handleClear(ACCOUNT_NUMBER_CLEAR.ACCOUNT_NUMBER_UPDATE);
		};
		// eslint-disable-next-line
	}, []);

	const onFinish = (data: IInputs) => {
		const { accountNumbers, circulars } = data;
		const formatAccountNumbers: {
			accountName?: string;
			accountNumber?: number;
			level: number;
			parentNumber?: number;
		}[] = [];

		accountNumbers.forEach((parent) => {
			formatAccountNumbers.push({
				accountName: parent.accountName,
				accountNumber: parent.accountNumber,
				level: 1,
			});
			parent.children?.forEach((child) => {
				formatAccountNumbers.push({
					...child,
					level: 2,
					parentNumber: parent.accountNumber,
				});
			});
		});
		if (isUpdatingAccountNumber) {
			props.actions.updateAccountNumber({
				_id: currentAccountNumber?._id as string,
				data: {
					circulars,
					accountNumbers: formatAccountNumbers as IAccountNumber[],
				},
			});
		} else {
			props.actions.createAccountNumber({
				circulars,
				accountNumbers: formatAccountNumbers as IAccountNumber[],
			});
		}
	};

	return (
		<React.Fragment>
			<Title level={4}>
				{' '}
				{param.circularsId && currentAccountNumber
					? `Chỉnh sửa tài khoản kế toán - ${currentAccountNumber.circulars}`
					: 'Tạo mới tài khoản kế toán'}
			</Title>
			<Form form={formInstance} layout="vertical" onFinish={onFinish} scrollToFirstError={true}>
				<Form.Item
					label="Tên thông tư"
					name="circulars"
					rules={[
						{
							required: true,
							message: 'Vui lòng tên thông tư!',
						},
						{
							max: 256,
						},
					]}
				>
					<Input placeholder="Tên thông tư" allowClear={true} />
				</Form.Item>
				<Form.List
					name="accountNumbers"
					rules={[
						{
							validator: async (_, names) => {
								if (!names || names.length < 1) {
									return Promise.reject(new Error('Có ít nhất 1 tài khoản'));
								}
							},
						},
					]}
				>
					{(fields, { add, remove }, { errors }) => {
						return (
							<>
								{fields.map((field) => (
									<Row key={field.key} gutter={16}>
										<Col lg={10}>
											<Form.Item
												{...field}
												label="Tên tài khoản cấp 1"
												name={[field.name, 'accountName']}
												fieldKey={[field.fieldKey, 'accountName']}
												rules={[
													{
														required: true,
														message: 'Vui lòng tên tài khoản!',
													},
												]}
											>
												<Input placeholder="Nhập tên tài khoản" allowClear={true} />
											</Form.Item>
										</Col>

										<Col lg={10}>
											<Form.Item
												{...field}
												label="Số tài khoản cấp 1"
												name={[field.name, 'accountNumber']}
												fieldKey={[field.fieldKey, 'accountNumber']}
												rules={[
													{
														required: true,
														message: 'Vui lòng số tài khoản!',
													},
												]}
											>
												<InputNumber className="w-100" min={0} step={1} />
											</Form.Item>
										</Col>

										<Col lg={4} className="d-flex align-items-center">
											<MinusCircleOutlined
												onClick={() => {
													remove(field.name);
												}}
											/>
										</Col>

										<Col lg={24}>
											<AccountNumberChildForm field={field} />
										</Col>
										<Divider />
									</Row>
								))}
								<Form.Item>
									<Button
										type="dashed"
										onClick={() => {
											add();
										}}
										icon={<PlusOutlined />}
										block={true}
									>
										Thêm tài khoản
									</Button>
									<Form.ErrorList errors={errors} />
								</Form.Item>
							</>
						);
					}}
				</Form.List>

				<div className="d-flex w-100 justify-content-end">
					<Space>
						<Button className="text-capitalize" onClick={() => props.history.goBack()}>
							Quay về
						</Button>
						<Button
							className="text-capitalize"
							type="primary"
							htmlType="submit"
							loading={isProcessing}
						>
							{isUpdatingAccountNumber ? 'Cập nhật' : 'Tạo'}
						</Button>
					</Space>
				</div>
			</Form>
		</React.Fragment>
	);
};
