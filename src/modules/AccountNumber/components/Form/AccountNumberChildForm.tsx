import React from 'react';
import { Form, Row, Col, Divider, Button, Input, InputNumber } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { FormListFieldData } from 'antd/lib/form/FormList';

export const AccountNumberChildForm = ({ field }: { field: FormListFieldData }) => {
	return (
		<Form.List name={[field.name, 'children']}>
			{(fields, { add, remove }) => {
				return (
					<>
						<Divider className="mt-0" orientation="left">
							Tài khoản cấp 2
						</Divider>
						{fields.map((field) => (
							<Row key={field.key} gutter={16}>
								<Col lg={10}>
									<Form.Item
										{...field}
										label="Tên tài khoản cấp 2"
										name={[field.name, 'accountName']}
										fieldKey={[field.fieldKey, 'accountName']}
										rules={[
											{
												required: true,
												message: 'Vui lòng tên tài khoản!',
											},
										]}
									>
										<Input placeholder="Nhập tên tài khoản" allowClear={true} />
									</Form.Item>
								</Col>

								<Col lg={10}>
									<Form.Item
										{...field}
										label="Số tài khoản cấp 2"
										name={[field.name, 'accountNumber']}
										fieldKey={[field.fieldKey, 'accountNumber']}
										rules={[
											{
												required: true,
												message: 'Vui lòng số tài khoản!',
											},
										]}
									>
										<InputNumber className="w-100" min={0} step={1} />
									</Form.Item>
								</Col>

								<Col lg={4} className="d-flex align-items-center">
									<MinusCircleOutlined
										onClick={() => {
											remove(field.name);
										}}
									/>
								</Col>
							</Row>
						))}
						<Form.Item>
							<Button
								type="dashed"
								onClick={() => {
									add();
								}}
								icon={<PlusOutlined />}
								block={true}
							>
								Thêm tài khoản con
							</Button>
						</Form.Item>
					</>
				);
			}}
		</Form.List>
	);
};
