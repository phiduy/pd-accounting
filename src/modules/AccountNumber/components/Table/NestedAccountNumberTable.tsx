import { Button, Form, Input, InputNumber, Popconfirm, Space, Table, Tooltip } from 'antd';
import React from 'react';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { IAccountNumber, IAccountNumberRecord } from '../../model/IAccountNumberState';
import { IAccountNumberProps } from '../../model/IAccountNumberProps';

interface NestedAccountNumberTableProps {
	record: IAccountNumberRecord;
	props: IAccountNumberProps;
}

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
	editing: boolean;
	dataIndex: string;
	title: any;
	inputType: 'number' | 'text';
	record: IAccountNumber;
	index: number;
	children: React.ReactNode;
}

const EditableCell: React.FC<EditableCellProps> = ({
	editing,
	dataIndex,
	title,
	inputType,
	record,
	index,
	children,
	...restProps
}) => {
	const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;

	return (
		<td {...restProps}>
			{editing ? (
				<Form.Item
					name={dataIndex}
					style={{ margin: 0 }}
					rules={[
						{
							required: true,
							message: `Vui lòng nhập trường dữ liệu ${title}!`,
						},
					]}
				>
					{inputNode}
				</Form.Item>
			) : (
				children
			)}
		</td>
	);
};

export const NestedAccountNumberEditableTable: React.FC<NestedAccountNumberTableProps> = ({
	props,
	record,
}) => {
	const { accountNumbers } = record;

	const [form] = Form.useForm();
	const [data, setData] = React.useState(accountNumbers);
	const [editingKey, setEditingKey] = React.useState('');

	const isEditing = (record: IAccountNumber) => record.key === editingKey;

	const edit = (record: Partial<IAccountNumber> & { key: React.Key }) => {
		form.setFieldsValue({ name: '', age: '', address: '', ...record });
		setEditingKey(record.key as string);
	};

	const cancel = () => {
		setEditingKey('');
	};

	const save = async (key: React.Key) => {
		try {
			const row = (await form.validateFields()) as IAccountNumber;

			const newData = [...data];
			const index = newData.findIndex((item) => key === item.key);
			if (index > -1) {
				const item = newData[index];
				newData.splice(index, 1, {
					...item,
					...row,
				});
				setData(newData);
				setEditingKey('');
			} else {
				newData.push(row);
				setData(newData);
				setEditingKey('');
			}
			const updateData: IAccountNumber[] = [];
			newData.forEach((item) => {
				if (item.children) {
					item.children.forEach((child) => {
						updateData.push(child);
					});
				}
				updateData.push(item);
			});

			// console.log('newData', newData);
			// console.log('updateData', updateData);

			props.actions.updateAccountNumber({
				_id: record._id as string,
				data: {
					// circulars,
					accountNumbers: updateData,
				},
			});
		} catch (errInfo) {
			console.log('Validate Failed:', errInfo);
		}
	};

	const remove = (record: Partial<IAccountNumber> & { key: React.Key }) => {
		console.log('record', record);
	};

	const columns = [
		{ title: 'Tên tài khoản', editable: true, dataIndex: 'accountName', key: 'accountName' },
		{ title: 'Số tài khoản', editable: true, dataIndex: 'accountNumber', key: 'accountNumber' },
		{
			title: 'Hàng động',
			dataIndex: 'operation',
			render: (_: any, record: IAccountNumber) => {
				const editable = isEditing(record);
				return editable ? (
					<span>
						<span
							className="cursor-pointer text-primary"
							onClick={() => save(record.key?.toString() as string)}
							style={{ marginRight: 8 }}
						>
							Lưu
						</span>
						<Popconfirm className="text-danger" title="Hủy thao tác?" onConfirm={cancel}>
							<span>Hủy</span>
						</Popconfirm>
					</span>
				) : (
					<Space>
						<Tooltip placement="top" title="Chỉnh sửa">
							<Button
								type="text"
								disabled={editingKey !== ''}
								onClick={() => edit(record)}
								icon={<EditOutlined className="text-primary" />}
							/>
						</Tooltip>
						<Tooltip placement="top" title="Xóa">
							<Popconfirm
								title="Bạn có chắc chắn xoá tài khoản kế toán này?"
								onConfirm={() => {
									remove(record);
								}}
								cancelText="Huỷ"
								okText="Xoá"
								disabled={editingKey !== ''}
							>
								<Button
									type="text"
									disabled={editingKey !== ''}
									onClick={() => edit(record)}
									icon={<DeleteOutlined className="text-danger" />}
								/>
							</Popconfirm>
						</Tooltip>
					</Space>
				);
			},
		},
	];

	const mergedColumns = columns.map((col) => {
		if (!col.editable) {
			return col;
		}
		return {
			...col,
			onCell: (record: IAccountNumber) => ({
				record,
				inputType: col.dataIndex === 'accountNumber' ? 'number' : 'text',
				dataIndex: col.dataIndex,
				title: col.title,
				editing: isEditing(record),
			}),
		};
	});

	return (
		<Form form={form} component={false}>
			<Table
				components={{
					body: {
						cell: EditableCell,
					},
				}}
				bordered={true}
				dataSource={data}
				columns={mergedColumns}
				rowClassName="editable-row"
				pagination={false}
			/>
		</Form>
	);
};
