import * as React from 'react';
import { Space, Table, Tooltip, Button, Popconfirm, Empty } from 'antd';
import { useMediaQuery } from 'react-responsive';
import { RouteComponentProps } from 'react-router-dom';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { IAccountNumberProps } from '../../model/IAccountNumberProps';
import { IAccountNumberRecord } from '../../model/IAccountNumberState';

import { IColumn } from '../../../../common/interfaces';
import { NestedAccountNumberEditableTable } from './NestedAccountNumberTable';

interface IProps extends IAccountNumberProps, RouteComponentProps {}

const columns = (props: IProps): IColumn[] => {
	return [
		{
			title: 'Thông tư',
			dataIndex: 'circulars',
			key: 'circulars',
		},
		{
			title: 'Hành động',
			dataIndex: 'operation',
			render: (text: any, record: IAccountNumberRecord) => (
				<Space>
					<Tooltip placement="top" title="Chỉnh sửa">
						<Button
							type="text"
							onClick={() => {
								props.actions.handleCurrentAccountNumber({
									type: 'update',
									accountNumberRecord: record,
								});
								props.history.push(`/management/account-number/edit/${record._id}`);
							}}
							icon={<EditOutlined className="text-primary" />}
						/>
					</Tooltip>
					<Tooltip placement="top" title="Xóa">
						<Popconfirm
							title="Bạn có chắc chắn xoá tài khoản kế toán này?"
							onConfirm={() => {
								props.actions.handleCurrentAccountNumber({
									type: 'delete',
									accountNumberRecord: record,
								});
								props.actions.deleteAccountNumber({ _id: record._id as string });
							}}
							cancelText="Huỷ"
							okText="Xoá"
						>
							<DeleteOutlined className="text-danger" />
						</Popconfirm>
					</Tooltip>
				</Space>
			),
		},
	];
};

interface AccountNumberTableProps {
	props: IProps;
	selectedRowKeys: number[];
	onSelectRowChange: (selectedRowKeys: any, selectedRows: any) => void;
}

export const AccountNumberTable: React.FC<AccountNumberTableProps> = ({
	props,
	selectedRowKeys,
	onSelectRowChange,
}) => {
	const isTabletPortrait = useMediaQuery({ query: '(max-width: 767px)' });
	const { accountNumberRecords, isLoading, isProcessing } = props.store.AccountNumberPage;
	return (
		<Table
			locale={{
				emptyText: <Empty description="Không có dữ liệu" />,
			}}
			scroll={{
				x: isTabletPortrait ? 1000 : 0,
			}}
			bordered={true}
			columns={columns(props)}
			rowSelection={{ selectedRowKeys, onChange: onSelectRowChange }}
			loading={isLoading || isProcessing}
			expandable={{
				expandedRowRender: (record) => (
					<NestedAccountNumberEditableTable props={props} record={record} />
				),
			}}
			pagination={false}
			dataSource={accountNumberRecords}
		/>
	);
};
