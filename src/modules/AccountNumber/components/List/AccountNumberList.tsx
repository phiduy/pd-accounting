import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Divider, Button, Modal } from 'antd';
import { PlusOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import { IAccountNumberProps } from '../../model/IAccountNumberProps';
import { CustomBreadCrumb } from '../../../../components';
import { AccountNumberTable } from '../Table';
import Hotkeys from 'react-hot-keys';

interface IProps extends RouteComponentProps, IAccountNumberProps {}

export const AccountNumberList: React.FC<IProps> = (props) => {
	const { accountNumberRecords } = props.store.AccountNumberPage;

	const [selectedRowKeys, setSelectedRowKeys] = React.useState<number[]>([]);
	const hasSelected = selectedRowKeys.length > 0;
	const onSelectRowChange = (selectedRowKeys: any, selectedRows: any) => {
		setSelectedRowKeys(selectedRowKeys);
	};

	React.useEffect(() => {
		props.actions.fetchAccountNumber({
			page: 0,
			limit: 10,
		});
		// eslint-disable-next-line
	}, []);

	const onDeleteRows = () => {
		Modal.confirm({
			title: 'Xác nhận',
			icon: <ExclamationCircleOutlined />,
			content: <div>Bạn có chắc chắn xóa {selectedRowKeys.length} dòng dữ liệu này</div>,
			okText: 'Xóa',
			cancelText: 'Hủy',
			onOk() {
				Modal.destroyAll();
			},
			onCancel() {
				console.log('Cancel');
			},
		});
	};

	const onKeyDown = (keyName: any, e: any, handle: any) => {
		switch (keyName) {
			case 'shift+n':
				props.history.push('/management/account-number/create');
				break;
			case 'shift+a':
				if (hasSelected) {
					setSelectedRowKeys([]);
				} else {
					setSelectedRowKeys(accountNumberRecords.map((item) => item.key));
				}
				break;
			case 'delete':
				onDeleteRows();
				break;

			default:
				break;
		}
	};

	return (
		<React.Fragment>
			<Hotkeys keyName="shift+a,shift+n,delete" onKeyDown={onKeyDown}>
				<CustomBreadCrumb
					title="Danh sách tài khoản kế toán"
					path="/management/account-number"
					routeName="Quản lý"
				/>
				<div className="box">
					<div className="box_content">
						<Button
							type="primary"
							icon={<PlusOutlined />}
							onClick={() => props.history.push('/management/account-number/create')}
						>
							Tạo mới tài khoản kế toán
						</Button>
						<Divider />
						<AccountNumberTable
							props={props}
							selectedRowKeys={selectedRowKeys}
							onSelectRowChange={onSelectRowChange}
						/>
					</div>
				</div>
			</Hotkeys>
		</React.Fragment>
	);
};
