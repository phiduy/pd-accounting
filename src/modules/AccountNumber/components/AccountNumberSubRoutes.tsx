import React from 'react';
import { Switch, Route, Redirect, useRouteMatch, RouteComponentProps } from 'react-router-dom';
import { IAccountNumberProps } from '../model/IAccountNumberProps';
import { Transition } from 'react-transition-group';
import { AccountNumberList } from './List';
import { CreateOrEditAccountNumber } from './CreateOrEdit';

interface IProps extends IAccountNumberProps, RouteComponentProps {}

export const AccountNumberSubRoutes: React.FC<IProps> = (props) => {
	const { url } = useRouteMatch();
	return (
		<Switch>
			<Route
				path={`${url}/list`}
				children={() => (
					<Transition timeout={500} in={true} appear={true}>
						{(status) => (
							<div className={`fade-in fade-in-${status}`}>
								<AccountNumberList {...props} />
							</div>
						)}
					</Transition>
				)}
			/>
			<Route
				path={`${url}/create`}
				children={() => (
					<Transition timeout={500} in={true} appear={true}>
						{(status) => (
							<div className={`fade-in fade-in-${status}`}>
								<CreateOrEditAccountNumber {...props} />
							</div>
						)}
					</Transition>
				)}
			/>
			<Route
				path={`${url}/edit/:circularsId`}
				children={() => (
					<Transition timeout={500} in={true} appear={true}>
						{(status) => (
							<div className={`fade-in fade-in-${status}`}>
								<CreateOrEditAccountNumber {...props} />
							</div>
						)}
					</Transition>
				)}
			/>
			<Redirect from="*" to={`${url}/list`} />
		</Switch>
	);
};
