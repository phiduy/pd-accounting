import * as React from 'react';
import { RouteComponentProps, useParams } from 'react-router-dom';
import { IAccountNumberProps } from '../../model/IAccountNumberProps';
import { CustomBreadCrumb } from '../../../../components';
import { CreateOrEditAccountNumberForm } from '../Form';

interface IProps extends RouteComponentProps, IAccountNumberProps {}

export const CreateOrEditAccountNumber: React.FC<IProps> = (props) => {
	const param: { circularsId: string } = useParams();
	const { currentAccountNumber } = props.store.AccountNumberPage;

	React.useEffect(() => {
		props.actions.fetchAccountNumber({
			page: 0,
			limit: 10,
		});
		// eslint-disable-next-line
	}, []);

	return (
		<React.Fragment>
			<CustomBreadCrumb
				title={
					param.circularsId && currentAccountNumber
						? 'Chỉnh sửa tài khoản kế toán'
						: 'Tạo mới tài khoản kế toán'
				}
				path="/management/account-number"
				routeName="Tài khoản kế toán"
			/>
			<div className="box">
				<div className="box_content">
					<CreateOrEditAccountNumberForm {...props} />
				</div>
			</div>
		</React.Fragment>
	);
};
