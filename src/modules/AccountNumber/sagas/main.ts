import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as AccountNumberApi from '../../../api/accountNumber';
import { message } from 'antd';

// Handle Fetch Account Number
function* handleFetchAccountNumber(action: any) {
	try {
		const res = yield call(AccountNumberApi.fetchAccountNumber, action.payload);
		if (res.status === 200) {
			yield delay(200);
			yield put(actions.fetchAccountNumberSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.fetchAccountNumberFail(error));
	}
}

// Handle Create Account Number
function* handleCreateAccountNumber(action: any) {
	try {
		const res = yield call(AccountNumberApi.createAccountNumber, action.payload);
		if (res.status === 200) {
			message.success('Tạo mới tài khoản kế toán thành công', 2);
			yield put(actions.createAccountNumberSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.createAccountNumberFail(error));
	}
}

// Handle Update Account Number
function* handleUpdateAccountNumber(action: any) {
	try {
		const res = yield call(AccountNumberApi.updateAccountNumber, action.payload);
		if (res.status === 200) {
			message.success('Cập nhật tài khoản kế toán thành công', 2);
			yield put(actions.updateAccountNumberSuccess(res.data.data));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.updateAccountNumberFail(error));
	}
}

// Handle Delete Account Number
function* handleDeleteAccountNumber(action: any) {
	try {
		const res = yield call(AccountNumberApi.deleteAccountNumber, action.payload);
		if (res.status === 200) {
			message.success('Xóa loại công ty thành công', 2);
			yield put(actions.deleteAccountNumberSuccess(res.data.data));
			yield put(actions.fetchAccountNumber({ page: 0, limit: 10 }));
		} else {
			throw res.data.errors;
		}
	} catch (error) {
		yield put(actions.deleteAccountNumberFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchFetchAccountNumber() {
	yield takeEvery(Keys.FETCH_ACCOUNT_NUMBER, handleFetchAccountNumber);
}
function* watchCreateAccountNumber() {
	yield takeEvery(Keys.CREATE_ACCOUNT_NUMBER, handleCreateAccountNumber);
}
function* watchUpdateAccountNumber() {
	yield takeEvery(Keys.UPDATE_ACCOUNT_NUMBER, handleUpdateAccountNumber);
}
function* watchDeleteAccountNumber() {
	yield takeEvery(Keys.DELETE_ACCOUNT_NUMBER, handleDeleteAccountNumber);
}
/*-----------------------------------------------------------------*/
const sagas = [
	watchFetchAccountNumber,
	watchCreateAccountNumber,
	watchUpdateAccountNumber,
	watchDeleteAccountNumber,
];
export default sagas;
