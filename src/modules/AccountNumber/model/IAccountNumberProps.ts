import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface IAccountNumberProps {
	store: IStore;
	actions: typeof Actions;
}
