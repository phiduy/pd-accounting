/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { IError } from '../../common/interfaces';
import { COMPANY_TYPE_MODAL, COMPANY_TYPE_CLEAR, ICompanyType } from './model/ICompanyTypeState';

export interface IToggleModal extends Action {
	readonly type: Keys.TOGGLE_MODAL;
	payload: {
		type: COMPANY_TYPE_MODAL;
	};
}

export interface IHandleCurrentCompanyType extends Action {
	readonly type: Keys.HANDLE_CURRENT_COMPANY_TYPE;
	payload: {
		type: 'update' | 'delete' | 'detail';
		companyType: ICompanyType;
	};
}

export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: COMPANY_TYPE_CLEAR;
	};
}

//#region Fetch Company Type IActions
export interface IFetchCompanyTypes extends Action {
	readonly type: Keys.FETCH_COMPANY_TYPES;
	payload: {
		page: number;
		limit: number;
	};
}

export interface IFetchCompanyTypesSuccess extends Action {
	readonly type: Keys.FETCH_COMPANY_TYPES_SUCCESS;
	payload: any[];
}

export interface IFetchCompanyTypesFail extends Action {
	readonly type: Keys.FETCH_COMPANY_TYPES_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region Create Company Type IActions
export interface ICreateCompanyType extends Action {
	readonly type: Keys.CREATE_COMPANY_TYPE;
	payload: { name: string };
}

export interface ICreateCompanyTypeSuccess extends Action {
	readonly type: Keys.CREATE_COMPANY_TYPE_SUCCESS;
	payload: any;
}

export interface ICreateCompanyTypeFail extends Action {
	readonly type: Keys.CREATE_COMPANY_TYPE_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region Update Company Type IActions
export interface IUpdateCompanyType extends Action {
	readonly type: Keys.UPDATE_COMPANY_TYPE;
	payload: { _id: string; companyType: ICompanyType };
}

export interface IUpdateCompanyTypeSuccess extends Action {
	readonly type: Keys.UPDATE_COMPANY_TYPE_SUCCESS;
	payload: any;
}

export interface IUpdateCompanyTypeFail extends Action {
	readonly type: Keys.UPDATE_COMPANY_TYPE_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region Delete Company Type IActions
export interface IDeleteCompanyType extends Action {
	readonly type: Keys.DELETE_COMPANY_TYPE;
	payload: { _id: string };
}

export interface IDeleteCompanyTypeSuccess extends Action {
	readonly type: Keys.DELETE_COMPANY_TYPE_SUCCESS;
	payload: any;
}

export interface IDeleteCompanyTypeFail extends Action {
	readonly type: Keys.DELETE_COMPANY_TYPE_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion
