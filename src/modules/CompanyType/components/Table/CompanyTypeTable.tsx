import * as React from 'react';
import { Space, Table, Tooltip, Button, Popconfirm, Empty } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { ICompanyTypeProps } from '../../model/ICompanyTypeProps';
import { IColumn } from '../../../../common/interfaces';
import { useMediaQuery } from 'react-responsive';
import { ICompanyType } from '../../model/ICompanyTypeState';

const columns = (props: ICompanyTypeProps): IColumn[] => {
	return [
		{
			title: 'Loại',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Số lượng công ty',
			dataIndex: 'numberOfCompany',
			key: 'numberOfCompany',
		},
		{
			title: 'Hành động',
			dataIndex: 'operation',
			render: (text: any, record: ICompanyType) => (
				<Space>
					<Tooltip placement="top" title="Chỉnh sửa">
						<Button
							type="text"
							onClick={() => {
								props.actions.handleCurrentCompanyType({
									type: 'update',
									companyType: record,
								});
							}}
							icon={<EditOutlined className="text-primary" />}
						/>
					</Tooltip>
					<Tooltip placement="top" title="Xóa">
						<Popconfirm
							title="Bạn có chắc chắn xoá công ty này?"
							onConfirm={() => {
								// props.actions.handleCurrentCompanyType({
								//   type: 'delete',
								//   companyType: record,
								// });
								props.actions.deleteCompanyType({ _id: record?._id as string });
							}}
							cancelText="Huỷ"
							okText="Xoá"
						>
							<DeleteOutlined className="text-danger" />
						</Popconfirm>
					</Tooltip>
				</Space>
			),
		},
	];
};

interface IProps extends ICompanyTypeProps {}

export const CompanyTypeTable: React.FC<IProps> = (props) => {
	const isTabletPortrait = useMediaQuery({ query: '(max-width: 767px)' });
	const { companyTypeRecords, isLoading, isProcessing } = props.store.CompanyTypePage;

	return (
		<Table
			locale={{
				emptyText: <Empty description="Không có dữ liệu" />,
			}}
			rowKey="_id"
			scroll={{
				x: isTabletPortrait ? 1000 : 0,
			}}
			columns={columns(props)}
			loading={isLoading || isProcessing}
			dataSource={companyTypeRecords}
		/>
	);
};
