import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Divider, Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { ICompanyTypeProps } from '../model/ICompanyTypeProps';
import { CustomBreadCrumb } from '../../../components';
import { CompanyTypeTable } from './Table';
import { CreateOrEditCompanyTypeForm } from './Form';
import { COMPANY_TYPE_MODAL } from '../model/ICompanyTypeState';

import './index.scss';

interface IProps extends RouteComponentProps, ICompanyTypeProps {}

export const CompanyTypePage: React.FC<IProps> = (props) => {
	React.useEffect(() => {
		props.actions.fetchCompanyTypes({
			page: 0,
			limit: 10,
		});
		// eslint-disable-next-line
	}, []);

	return (
		<React.Fragment>
			<CustomBreadCrumb
				title="Danh sách loại công ty"
				path="/management/company-type"
				routeName="Quản lý"
			/>
			<div className="box">
				<div className="box_content">
					<Button
						type="primary"
						icon={<PlusOutlined />}
						onClick={() =>
							props.actions.toggleModal({
								type: COMPANY_TYPE_MODAL.CREATE_EDIT_COMPANY_TYPE,
							})
						}
					>
						Tạo mới loại công ty
					</Button>
					<Divider />
					<CompanyTypeTable {...props} />
				</div>
			</div>
			<CreateOrEditCompanyTypeForm {...props} />
		</React.Fragment>
	);
};
