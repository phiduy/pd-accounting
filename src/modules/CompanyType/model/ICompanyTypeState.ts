export enum COMPANY_TYPE_MODAL {
	CREATE_EDIT_COMPANY_TYPE = 1,
}

export enum COMPANY_TYPE_CLEAR {
	COMPANY_TYPE_UPDATE = 1,
}

export interface ICompanyType {
	name: string;
	_id?: string;
	numberOfCompany?: number;
}

export interface ICompanyTypeState {
	toggleModalCreateEditCompanyType: boolean;
	isUpdatingCompanyType: boolean;
	companyTypeRecords: ICompanyType[];
	currentCompanyType?: ICompanyType;
	isProcessing: boolean;
	isLoading: boolean;
}

// InitialState
export const initialState: ICompanyTypeState = {
	toggleModalCreateEditCompanyType: false,
	isUpdatingCompanyType: false,
	companyTypeRecords: [],
	isLoading: false,
	isProcessing: false,
};
