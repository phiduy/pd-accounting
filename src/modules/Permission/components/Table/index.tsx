import * as React from 'react';
import { IPermissionProps } from '../../model/IPermissionProps';
import { Switch, Table, Empty } from 'antd';
import { IPermissionObj, IColumn } from '../../../../common/interfaces';

const columns = (props: IPermissionProps): IColumn[] => {
	return [
		{
			title: 'Vai trò',
			dataIndex: 'title',
			key: 'title',
		},
		{
			title: 'Cho phép',
			// shouldCellUpdate: (record, prevRecord) => {
			//     console.log(record.children, prevRecord);
			//     if (record !== prevRecord) return true;
			// },
			render: (text: any, record: IPermissionObj) => {
				if (!record.hasOwnProperty('children')) {
					return (
						<Switch
							defaultChecked={record.isActive}
							onChange={(checked) => {
								props.actions.handlePermissionSwitch({
									value: {
										_id: record._id,
										permissionKey: record.key,
									},
									status: checked,
								});
							}}
						/>
					);
				}
				return null;
			},
		},
	];
};

interface IProps extends IPermissionProps {}

export const PermissionTable: React.FC<IProps> = (props) => {
	const { permissionRecords, isLoadingPermissions } = props.store.PermissionPage;
	// React.useEffect(() => {}, [permissionRecords]);

	return (
		<Table
			locale={{ emptyText: <Empty description="Không có dữ liệu" /> }}
			loading={isLoadingPermissions}
			dataSource={permissionRecords}
			columns={columns(props)}
			rowKey="_id"
			expandable={{
				expandRowByClick: true,
				defaultExpandAllRows: true,
			}}
			pagination={false}
		/>
	);
};
