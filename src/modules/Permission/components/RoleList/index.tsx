import * as React from 'react';
import { Button, Empty, List, Badge } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { PERMISSION_MODAL, PERMISSION_CLEAR } from '../../model/IPermissionState';
import { IPermissionProps } from '../../model/IPermissionProps';
// import { isGranted } from '../../../../services/checkPermission';
import { IRole } from '../../../../common/interfaces';

interface IProps extends IPermissionProps {}

export const RoleList: React.FC<IProps> = (props) => {
	const { isProcessing, isLoadingRoles, roleRecords, roleSelected } = props.store.PermissionPage;

	const onChangeRole = (item: any) => {
		props.actions.handleRoleSelect(item);
		setTimeout(() => {
			props.actions.handleClear(PERMISSION_CLEAR.PERMISSIONS_LOADING);
		}, 1000);
	};

	return (
		<div className="role__list">
			<div
				style={{
					width: '100%',
					margin: '10px 0',
				}}
			>
				<Button
					icon={<PlusOutlined />}
					type="primary"
					onClick={() =>
						!isProcessing &&
						props.actions.toggleModal({
							type: PERMISSION_MODAL.ADD_EDIT_ROLE,
						})
					}
					// disabled={!isGranted('roleCreate')}
				>
					Tạo mới vai trò
				</Button>
			</div>
			<List
				loading={isLoadingRoles}
				header={null}
				footer={null}
				bordered={true}
				locale={{
					emptyText: <Empty description="Không có dữ liệu" />,
				}}
				dataSource={roleRecords}
				renderItem={(item: IRole) => (
					<List.Item
						onClick={() => {
							if ((roleSelected && item._id !== roleSelected._id) || !isProcessing) {
								onChangeRole(item);
							}
						}}
						className={roleSelected && item._id === roleSelected._id ? 'active' : ''}
					>
						<Badge dot={true} status={item.status === 'enable' ? 'success' : 'warning'} />
						{item.roleName}
					</List.Item>
				)}
			/>
		</div>
	);
};
