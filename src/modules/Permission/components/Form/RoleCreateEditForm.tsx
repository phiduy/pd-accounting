import * as React from 'react';
import { Typography, Form, Modal, Input, Button } from 'antd';
import { IPermissionProps } from '../../model/IPermissionProps';
import { PERMISSION_MODAL } from '../../model/IPermissionState';
const { Title } = Typography;

interface IInputs {
	roleName: string;
}

export const RoleCreateEditForm: React.FC<IPermissionProps> = (props) => {
	const [formInstance] = Form.useForm();
	const { toggleModalAddEditRole, isProcessing } = props.store.PermissionPage;

	const onFinish = (data: IInputs) => {
		props.actions.postRole({
			...data,
			rolePermissions: [],
		});
	};

	return (
		<React.Fragment>
			<Modal
				visible={toggleModalAddEditRole}
				onCancel={() => {
					formInstance.resetFields();
					props.actions.toggleModal({
						type: PERMISSION_MODAL.ADD_EDIT_ROLE,
					});
				}}
				maskClosable={false}
				destroyOnClose={true}
				footer={null}
			>
				<Form form={formInstance} name="role-info" layout="vertical" onFinish={onFinish}>
					<Title level={4}>Thông tin vai trò</Title>
					<Form.Item
						name="roleName"
						label="Tên vai trò"
						hasFeedback={true}
						rules={[
							{
								required: true,
							},
						]}
						children={
							<Input placeholder="Nhập tên vai trò" allowClear={true} disabled={isProcessing} />
						}
					/>
					<Form.Item
						children={
							<div className="d-flex justify-content-end w-100">
								<Button type="primary" htmlType="submit" loading={isProcessing}>
									Tạo vai trò
								</Button>
							</div>
						}
					/>
				</Form>
			</Modal>
		</React.Fragment>
	);
};
