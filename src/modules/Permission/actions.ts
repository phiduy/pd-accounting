import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { PERMISSION_CLEAR } from './model/IPermissionState';
import { IRole } from '../../common/interfaces';

export const toggleModal = ({ type }: any): IActions.IToggleModal => {
	return {
		type: Keys.TOGGLE_MODAL,
		payload: {
			type,
		},
	};
};

export const handleClear = (type: PERMISSION_CLEAR): IActions.IHandleClear => {
	return {
		type: Keys.HANDLE_CLEAR,
		payload: {
			type,
		},
	};
};

export const handleRoleNameChange = (data: string): IActions.IHandleRoleNameChange => {
	return {
		type: Keys.HANDLE_ROLE_NAME_CHANGE,
		payload: {
			value: data,
		},
	};
};

export const handleRoleSelect = (value: IRole): IActions.IHandleRoleSelect => {
	return {
		type: Keys.HANDLE_ROLE_SELECT,
		payload: { ...value },
	};
};

export const handlePermissionSwitch = ({
	value,
	status,
}: {
	value: { _id: string; permissionKey: string };
	status: boolean;
}): IActions.IHandlePermissionSwitch => {
	return {
		type: Keys.HANDLE_PERMISSION_SWITCH,
		payload: {
			value,
			status,
		},
	};
};

//#region Get Roles Actions
export const getRoles = ({
	page,
	limit,
}: {
	page?: number;
	limit?: number;
}): IActions.IGetRoles => {
	return {
		type: Keys.GET_ROLES,
		payload: {
			page,
			limit,
		},
	};
};
export const getRolesSuccess = (res: any): IActions.IGetRolesSuccess => {
	return {
		type: Keys.GET_ROLES_SUCCESS,
		payload: res,
	};
};
export const getRolesFail = (res: any): IActions.IGetRolesFail => {
	return {
		type: Keys.GET_ROLES_FAIL,
		payload: res,
	};
};
//#endregion

//#region Post Role Actions
export const postRole = ({
	roleName,
	rolePermissions,
}: {
	roleName: string;
	rolePermissions: any;
}): IActions.IPostRole => {
	return {
		type: Keys.POST_ROLE,
		payload: {
			roleName,
			rolePermissions,
		},
	};
};
export const postRoleSuccess = (res: any): IActions.IPostRoleSuccess => {
	return {
		type: Keys.POST_ROLE_SUCCESS,
		payload: res,
	};
};
export const postRoleFail = (res: any): IActions.IPostRoleFail => {
	return {
		type: Keys.POST_ROLE_FAIL,
		payload: res,
	};
};
//#endregion

//#region Put Role Actions
export const putRole = (data: {
	_id: string;
	roleName: string;
	rolePermissions: any;
}): IActions.IPutRole => {
	return {
		type: Keys.PUT_ROLE,
		payload: {
			...data,
		},
	};
};
export const putRoleSuccess = (res: any): IActions.IPutRoleSuccess => {
	return {
		type: Keys.PUT_ROLE_SUCCESS,
		payload: res,
	};
};
export const putRoleFail = (res: any): IActions.IPutRoleFail => {
	return {
		type: Keys.PUT_ROLE_FAIL,
		payload: res,
	};
};
//#endregion

//#region Delete Role Actions
export const deleteRole = (data: { _id: string }): IActions.IDeleteRole => {
	return {
		type: Keys.DELETE_ROLE,
		payload: {
			...data,
		},
	};
};
export const deleteRoleSuccess = (res: any): IActions.IDeleteRoleSuccess => {
	return {
		type: Keys.DELETE_ROLE_SUCCESS,
		payload: res,
	};
};
export const deleteRoleFail = (res: any): IActions.IDeleteRoleFail => {
	return {
		type: Keys.DELETE_ROLE_FAIL,
		payload: res,
	};
};
//#endregion

//#region Change Role's Status Actions
export const changeRoleStatus = (data: {
	_id: string;
	type: string;
}): IActions.IChangeRoleStatus => {
	return {
		type: Keys.CHANGE_ROLE_STATUS,
		payload: {
			...data,
		},
	};
};
export const changeRoleStatusSuccess = (): IActions.IChangeRoleStatusSuccess => {
	return {
		type: Keys.CHANGE_ROLE_STATUS_SUCCESS,
	};
};
export const changeRoleStatusFail = (res: any): IActions.IChangeRoleStatusFail => {
	return {
		type: Keys.CHANGE_ROLE_STATUS_FAIL,
		payload: res,
	};
};
//#endregion

//#region Get Permissions Actions
export const getPermissions = (): IActions.IGetPermissions => {
	return {
		type: Keys.GET_PERMISSIONS,
	};
};
export const getPermissionsSuccess = (res: any): IActions.IGetPermissionsSuccess => {
	return {
		type: Keys.GET_PERMISSIONS_SUCCESS,
		payload: res,
	};
};
export const getPermissionsFail = (res: any): IActions.IGetPermissionsFail => {
	return {
		type: Keys.GET_PERMISSIONS_FAIL,
		payload: res,
	};
};
//#endregion
