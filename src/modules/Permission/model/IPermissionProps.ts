import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface IPermissionProps {
	store: IStore;
	actions: typeof Actions;
}
