import * as React from 'react';
import { Form, Input, Button, Modal, Typography, InputNumber } from 'antd';
import { ITaxProps } from '../../model/ITaxProps';
import { TAX_MODAL, TAX_CLEAR } from '../../model/ITaxState';

const { Title } = Typography;

interface IProps extends ITaxProps {}

interface IInputs {
	name: string;
	tax: number;
}

export const CreateOrEditTaxForm = (props: IProps) => {
	const [formInstance] = Form.useForm();
	const { toggleModalAddEditTax, curTax, isProcessing, isUpdatingTax } = props.store.TaxPage;

	React.useEffect(() => {
		if (isUpdatingTax && curTax) {
			formInstance.setFieldsValue({
				name: curTax.name,
				tax: curTax.tax,
			});
		}
		// eslint-disable-next-line
	}, [isUpdatingTax, formInstance, curTax]);

	const onFinish = (data: IInputs) => {
		if (isUpdatingTax) {
			props.actions.updateTax({
				_id: curTax?._id as string,
				tax: data,
			});
		} else {
			props.actions.createTax(data);
		}
	};

	return (
		<Modal
			visible={toggleModalAddEditTax}
			onCancel={() => {
				formInstance.resetFields();
				props.actions.handleClear(TAX_CLEAR.TAX_UPDATE);
				props.actions.toggleModal({
					type: TAX_MODAL.ADD_EDIT_TAX,
				});
			}}
			afterClose={() => {
				formInstance.resetFields();
			}}
			maskClosable={false}
			footer={null}
		>
			<Title level={4}>Thông tin thuế</Title>
			<Form form={formInstance} layout="vertical" onFinish={onFinish}>
				<Form.Item
					name="name"
					label="Thuế"
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
					]}
					children={<Input allowClear={true} disabled={isProcessing} />}
				/>
				<Form.Item
					name="tax"
					label="Phí thu"
					rules={[
						{
							required: true,
						},
					]}
					children={
						<InputNumber
							className="w-100"
							min={0}
							max={10000}
							formatter={(value) => `${value}%`}
							parser={(value: any) => value.replace('%', '')}
						/>
					}
				/>
				<Form.Item
					children={
						<div className="d-flex w-100 justify-content-end">
							<Button type="primary" htmlType="submit" loading={isProcessing} className="w-120px">
								{isUpdatingTax ? 'Cập nhật' : 'Thêm mới'}
							</Button>
						</div>
					}
				/>
			</Form>
		</Modal>
	);
};
