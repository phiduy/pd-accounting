import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { ITaxRecord, TAX_MODAL, TAX_CLEAR } from './model/ITaxState';

export const toggleModal = (data: { type: TAX_MODAL }): IActions.IToggleModal => {
	return {
		type: Keys.TOGGLE_MODAL,
		payload: {
			...data,
		},
	};
};

export const handleClear = (type: TAX_CLEAR): IActions.IHandleClear => {
	return {
		type: Keys.HANDLE_CLEAR,
		payload: {
			type,
		},
	};
};

export const handleCurTax = (data: {
	type: 'update' | 'delete';
	tax: ITaxRecord;
}): IActions.IHandleCurTax => {
	return {
		type: Keys.HANDLE_CUR_TAX,
		payload: {
			...data,
		},
	};
};

//#region GET Tax Actions
export const fetchTaxes = (): IActions.IFetchTaxes => {
	return {
		type: Keys.FETCH_TAXES,
	};
};
export const fetchTaxesSuccess = (res: any): IActions.IFetchTaxesSuccess => {
	return {
		type: Keys.FETCH_TAXES_SUCCESS,
		payload: res,
	};
};
export const fetchTaxesFail = (res: any): IActions.IFetchTaxesFail => {
	return {
		type: Keys.FETCH_TAXES_FAIL,
		payload: res,
	};
};
//#endregion

//#region CREATE Tax Actions
export const createTax = (data: ITaxRecord): IActions.ICreateTax => {
	return {
		type: Keys.CREATE_TAX,
		payload: data,
	};
};
export const createTaxSuccess = (res: any): IActions.ICreateTaxSuccess => {
	return {
		type: Keys.CREATE_TAX_SUCCESS,
		payload: res,
	};
};
export const createTaxFail = (res: any): IActions.ICreateTaxFail => {
	return {
		type: Keys.CREATE_TAX_FAIL,
		payload: res,
	};
};
//#endregion

//#region UPDATE Tax Actions
export const updateTax = (data: { _id: string; tax: ITaxRecord }): IActions.IUpdateTax => {
	return {
		type: Keys.UPDATE_TAX,
		payload: {
			...data,
		},
	};
};
export const updateTaxSuccess = (res: any): IActions.IUpdateTaxSuccess => {
	return {
		type: Keys.UPDATE_TAX_SUCCESS,
		payload: res,
	};
};
export const updateTaxFail = (res: any): IActions.IUpdateTaxFail => {
	return {
		type: Keys.UPDATE_TAX_FAIL,
		payload: res.errors,
	};
};
//#endregion
