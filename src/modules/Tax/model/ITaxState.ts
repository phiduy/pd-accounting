export enum TAX_MODAL {
	ADD_EDIT_TAX = 1,
}

export enum TAX_CLEAR {
	TAX_UPDATE = 0,
}

export interface ITaxRecord {
	key?: number;
	_id?: string;
	name: string;
	tax: number;
}

export interface ITaxState {
	taxRecords: ITaxRecord[];
	toggleModalAddEditTax: boolean;
	isLoadingTax: boolean;
	isProcessing: boolean;
	isUpdatingTax: boolean;
	curTax?: ITaxRecord;
}
// InitialState
export const initialState: ITaxState = {
	taxRecords: [],
	toggleModalAddEditTax: false,
	isLoadingTax: false,
	isProcessing: false,
	isUpdatingTax: false,
};
