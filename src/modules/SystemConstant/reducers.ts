import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { ISystemConstantState, initialState } from './model/ISystemConstantState';

export const name = 'SystemConstant';

export const reducer: Reducer<ISystemConstantState, ActionTypes> = (
	state = initialState,
	action
) => {
	switch (action.type) {
		case Keys.FETCH_SYSTEM_CONSTANTS:
			return onFetchSystemConstants(state, action);
		case Keys.FETCH_SYSTEM_CONSTANTS_SUCCESS:
			return onFetchSystemConstantsSuccess(state, action);
		case Keys.FETCH_SYSTEM_CONSTANTS_FAIL:
			return onFetchSystemConstantsFail(state, action);

		case Keys.UPDATE_SYSTEM_CONSTANTS:
			return onUpdateSystemConstants(state, action);
		case Keys.UPDATE_SYSTEM_CONSTANTS_SUCCESS:
			return onUpdateSystemConstantsSuccess(state, action);
		case Keys.UPDATE_SYSTEM_CONSTANTS_FAIL:
			return onUpdateSystemConstantsFail(state, action);
		default:
			return state;
	}
};

// IActions: the interface of current action

const onFetchSystemConstants = (
	state: ISystemConstantState,
	action: IActions.IFetchSystemConstants
) => {
	return {
		...state,
		isLoading: true,
	};
};
const onFetchSystemConstantsSuccess = (
	state: ISystemConstantState,
	action: IActions.IFetchSystemConstantsSuccess
) => {
	return {
		...state,
		isLoading: false,
		systemConstants: action.payload,
	};
};
const onFetchSystemConstantsFail = (
	state: ISystemConstantState,
	action: IActions.IFetchSystemConstantsFail
) => {
	return {
		...state,
		isLoading: false,
	};
};

const onUpdateSystemConstants = (
	state: ISystemConstantState,
	action: IActions.IUpdateSystemConstants
) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onUpdateSystemConstantsSuccess = (
	state: ISystemConstantState,
	action: IActions.IUpdateSystemConstantsSuccess
) => {
	return {
		...state,
		isProcessing: false,
		systemConstants: action.payload,
	};
};
const onUpdateSystemConstantsFail = (
	state: ISystemConstantState,
	action: IActions.IUpdateSystemConstantsFail
) => {
	return {
		...state,
		isProcessing: false,
	};
};
