/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IHandleClear
	| IActions.IFetchSystemConstants
	| IActions.IFetchSystemConstantsSuccess
	| IActions.IFetchSystemConstantsFail
	| IActions.IUpdateSystemConstants
	| IActions.IUpdateSystemConstantsSuccess
	| IActions.IUpdateSystemConstantsFail;

export default ActionTypes;
