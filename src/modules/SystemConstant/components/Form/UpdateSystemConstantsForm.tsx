import React from 'react';
import { Button, Form, InputNumber, Row, Col } from 'antd';
import { ISystemConstantProps } from '../../model/ISystemConstantProps';
import { ISystemConstant } from '../../model/ISystemConstantState';

interface IProps extends ISystemConstantProps {}

export const UpdateSystemConstantsForm: React.FC<IProps> = ({ actions, store }) => {
	const [formInstance] = Form.useForm();
	const { isProcessing, systemConstants } = store.SystemConstant;

	React.useEffect(() => {
		if (systemConstants) {
			formInstance.setFieldsValue({
				userTotalFreeCompany: systemConstants.userTotalFreeCompany,
				userTotalFreeBill: systemConstants.userTotalFreeBill,
			});
		}
		// eslint-disable-next-line
	}, [systemConstants]);

	const onFinish = (data: ISystemConstant) => {
		actions.updateSystemConstants(data);
	};
	return (
		<Form form={formInstance} layout="vertical" onFinish={onFinish}>
			<Row>
				<Col xl={12} lg={12}>
					<Form.Item
						label="Giới hạn số lượng công ty của gói miễn phí"
						name="userTotalFreeCompany"
						children={
							<InputNumber
								className="w-100"
								min={1}
								max={1000}
								step={1}
								disabled={isProcessing}
								placeholder="Nhập số lượng công ty"
							/>
						}
					/>
					<Form.Item
						label="Giới hạn số lượng phiếu chi của gói miễn phí"
						name="userTotalFreeBill"
						children={
							<InputNumber
								className="w-100"
								min={1}
								max={1000}
								step={1}
								disabled={isProcessing}
								placeholder="Nhập số lượng công ty"
							/>
						}
					/>
					<Form.Item
						children={
							<Button
								className="text-capitalize"
								type="primary"
								htmlType="submit"
								loading={isProcessing}
							>
								Cập nhật
							</Button>
						}
					/>
				</Col>
			</Row>
		</Form>
	);
};
