import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface ISystemConstantProps {
	store: IStore;
	actions: typeof Actions;
}
