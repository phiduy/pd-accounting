/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { IError } from '../../common/interfaces';
import { ICompany, COMPANY_MODAL, IProvince, ICompanyType } from './model/ICompanyState';

export interface IToggleModal extends Action {
	readonly type: Keys.TOGGLE_MODAL;
	payload: {
		type: COMPANY_MODAL;
	};
}

export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: string;
	};
}

//#region GET Locations IActions
export interface IGetLocations extends Action {
	readonly type: Keys.GET_LOCATIONS;
	payload: {
		level: number;
		parentCode: number | string;
		preLoad?: boolean;
	};
}
export interface IGetLocationsSuccess extends Action {
	readonly type: Keys.GET_LOCATIONS_SUCCESS;
	payload: {
		data: IProvince[];
		preLoad: boolean;
	};
}
export interface IGetLocationsFail extends Action {
	readonly type: Keys.GET_LOCATIONS_FAIL;
	payload: IError[];
}
//#endregion

//#region Fetch Company Type IActions
export interface IFetchCompanyTypes extends Action {
	readonly type: Keys.FETCH_COMPANY_TYPES;
	payload: {
		page: number;
		limit: number;
	};
}

export interface IFetchCompanyTypesSuccess extends Action {
	readonly type: Keys.FETCH_COMPANY_TYPES_SUCCESS;
	payload: ICompanyType[];
}

export interface IFetchCompanyTypesFail extends Action {
	readonly type: Keys.FETCH_COMPANY_TYPES_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region Fetch Company IActions
export interface IFetchCompany extends Action {
	readonly type: Keys.FETCH_COMPANY;
	payload: {
		page: number;
		limit: number;
	};
}

export interface IFetchCompanySuccess extends Action {
	readonly type: Keys.FETCH_COMPANY_SUCCESS;
	payload: ICompany;
}

export interface IFetchCompanyFail extends Action {
	readonly type: Keys.FETCH_COMPANY_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region Create Company IActions
export interface ICreateCompany extends Action {
	readonly type: Keys.CREATE_COMPANY;
	payload: ICompany;
}

export interface ICreateCompanySuccess extends Action {
	readonly type: Keys.CREATE_COMPANY_SUCCESS;
	payload: ICompany;
}

export interface ICreateCompanyFail extends Action {
	readonly type: Keys.CREATE_COMPANY_FAIL;
	payload: {
		errors: IError[];
	};
}
//#endregion
