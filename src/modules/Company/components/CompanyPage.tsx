import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Divider, Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { ICompanyProps } from '../model/ICompanyProps';
import { CustomBreadCrumb } from '../../../components';
import { CompanyTable } from './Table';
import { CreateOrEditCompanyForm } from './Form';
import { COMPANY_MODAL } from '../model/ICompanyState';

import './index.scss';

interface IProps extends RouteComponentProps, ICompanyProps {}

export const CompanyPage: React.FC<IProps> = (props) => {
	React.useEffect(() => {
		props.actions.getLocations({
			level: 1,
			parentCode: 0,
		});
		props.actions.fetchCompany({
			page: 0,
			limit: 10,
		});
		props.actions.fetchCompanyTypes({
			page: 0,
			limit: 10,
		});
		// eslint-disable-next-line
	}, []);

	return (
		<React.Fragment>
			<CustomBreadCrumb title="Danh sách công ty" path="/management/company" routeName="Quản lý" />
			<div className="box">
				<div className="box_content">
					<Button
						type="primary"
						icon={<PlusOutlined />}
						onClick={() =>
							props.actions.toggleModal({
								type: COMPANY_MODAL.CREATE_EDIT_COMPANY,
							})
						}
					>
						Tạo mới công ty
					</Button>
					<Divider />
					<CompanyTable {...props} />
				</div>
			</div>
			<CreateOrEditCompanyForm {...props} />
		</React.Fragment>
	);
};
