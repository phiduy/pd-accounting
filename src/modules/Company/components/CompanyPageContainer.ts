import IStore from '../../../redux/store/IStore';
import * as actions from '../actions';
import ActionTypes from '../actionTypes';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { CompanyPage } from './CompanyPage';

function mapStateToProps(store: IStore) {
	return {
		store: store,
	};
}

function mapDispatchToProps(dispatch: Dispatch<ActionTypes>) {
	return {
		actions: bindActionCreators(actions, dispatch),
	};
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CompanyPage));
