import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface ICurrencyProps {
	store: IStore;
	actions: typeof Actions;
}
