import React from 'react';
import { Input, Button, Typography, Modal, Form, Space, InputNumber } from 'antd';
import { ICurrencyProps } from '../../model/ICurrencyProps';
import { CURRENCY_TYPE_MODAL, CURRENCY_TYPE_CLEAR } from '../../model/ICurrencyState';

const { Title } = Typography;

interface IProps extends ICurrencyProps {}

interface IInputs {
	name: string;
	defaultExchangeRate: string;
}

export const CreateOrEditCurrencyTypeForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const {
		isProcessing,
		currentCurrencyType,
		isUpdatingCurrency,
		toggleModalCreateEditCurrency,
	} = props.store.CurrencyPage;

	React.useEffect(() => {
		if (currentCurrencyType) {
			formInstance.setFieldsValue({
				name: currentCurrencyType.name,
				defaultExchangeRate: currentCurrencyType.defaultExchangeRate,
			});
		}
	}, [isUpdatingCurrency, formInstance, currentCurrencyType]);

	const onFinish = (data: IInputs) => {
		if (isUpdatingCurrency) {
			props.actions.updateCurrency({
				_id: currentCurrencyType?._id as string,
				currency: data,
			});
		} else {
			props.actions.createCurrency(data);
		}
	};

	return (
		<Modal
			visible={toggleModalCreateEditCurrency}
			onCancel={() => {
				formInstance.resetFields();
				if (isUpdatingCurrency) {
					props.actions.handleClear(CURRENCY_TYPE_CLEAR.CURRENCY_TYPE_UPDATE);
				}
				props.actions.toggleModal({
					type: CURRENCY_TYPE_MODAL.CREATE_EDIT_CURRENCY_TYPE,
				});
			}}
			afterClose={() => {
				formInstance.resetFields();
			}}
			maskClosable={false}
			footer={null}
		>
			<Title level={4}>Tạo mới loại tiền tệ</Title>
			<Form form={formInstance} layout="vertical" onFinish={onFinish}>
				<Form.Item
					label="Tên loại tiền tệ"
					name="name"
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
					]}
					children={
						<Input placeholder="Nhập tên loại tiền tệ" allowClear={true} disabled={isProcessing} />
					}
				/>
				<Form.Item
					label="Tỉ giá"
					name="defaultExchangeRate"
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
					]}
					children={
						<InputNumber
							formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
							className="w-100"
							min={100}
							step={1}
							disabled={isProcessing}
							placeholder="Nhập tỉ giá"
						/>
					}
				/>

				<div className="d-flex w-100 justify-content-end">
					<Space>
						<Button
							className="text-capitalize"
							onClick={() => {
								props.actions.toggleModal({
									type: CURRENCY_TYPE_MODAL.CREATE_EDIT_CURRENCY_TYPE,
								});
							}}
						>
							Đóng
						</Button>
						<Button
							className="text-capitalize"
							type="primary"
							htmlType="submit"
							loading={isProcessing}
						>
							{isUpdatingCurrency ? 'Cập nhật' : 'Tạo'}
						</Button>
					</Space>
				</div>
			</Form>
		</Modal>
	);
};
