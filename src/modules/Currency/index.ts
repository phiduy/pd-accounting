/**
 * @file index
 * Please import some config when register new module
 * Some base file neeed to update
 * * src/redux/rootReducers.js
 * * src/redux/rootSagas.js
 */

import sagas from './sagas';
import { reducer, name } from './reducers';
import { ICurrencyState, initialState } from './model/ICurrencyState';

export { name, sagas, initialState };
export type { ICurrencyState };

export default reducer;
