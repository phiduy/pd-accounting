import { IError, IValidateMessage } from './interfaces';
// eslint-disable-all-line
export const dateFormat = 'DD/MM/YYYY';

export const SSL = 'https://';

export const errorRoleMessages: IError[] = [
	{
		code: 'usersIsBlocked',
		message: 'Tài khoản không có quyền để thực hiện',
	},
	{
		code: 'adminDidNotHavePermissions',
		message: 'Tài khoản quản trị không có quyền để thực hiện',
	},
];

export const errorAdminAccountMessages: IError[] = [
	{
		code: 'adminAccountNotFound',
		message: 'Không tìm thấy tài khoản quản trị !',
	},
	{
		code: 'adminAccountDidNotHavePermissions',
		message: 'Tài khoản không có quyền thực hiện chức năng !',
	},
	{
		code: 'adminAccountCanOnlySetRoleForAnotherAccount',
		message: 'Tài khoản chỉ có thể thay đổi vai trò tài khoản khác ',
	},
	{
		code: 'adminAccountWrongPassword',
		message: 'Tài khoản sai mật khẩu !',
	},
	{
		code: 'adminAccountExisted',
		message: 'Tên tài khoản đã tồn tại !',
	},
	{
		code: 'adminAccountNotFound',
		message: 'Không tìm thấy tài khoản quản trị !',
	},
];

export const errorCategoryMessages: IError[] = [
	{
		code: 'categoryExisted',
		message: 'Danh mục đã tồn tại !',
	},
];

export const errorSetupMessages: IError[] = [
	{
		code: 'configDepositPackExisted',
		message: 'Gói tiền đã tồn tại !',
	},
	{
		code: 'configFeesExisted',
		message: 'Loại phí đã tồn tại!',
	},
	{
		code: 'invalid',
		message: 'Phí mặc định không hợp lệ',
	},
];

export const errorEmployeeMessages: IError[] = [
	{
		code: 'employeesWrongAddress',
		message: 'Địa chỉ của nhân viên không hợp lệ !',
	},
	{
		code: 'employeesWrongPassword',
		message: 'Mật khẩu của nhân viên không đúng !',
	},
	{
		code: 'employeesPhoneOrIdentityExisted',
		message: 'Số điện thoại hoặc chứng minh nhân đã tồn tại !',
	},
	{
		code: 'employeesAccountUserNameExisted',
		message: 'Tên tài khoản đã tồn tại !',
	},
	{
		code: 'employeesUsernameNotFound',
		message: 'Tên tài khoản không tồn tại !',
	},
	{
		code: 'invalid',
		message: 'Phí mặc định không hợp lệ !',
	},
];

export const errorBankCardMessages: IError[] = [
	{
		code: 'companyBankingExisted',
		message: 'Thẻ ngân hàng đã tồn tại !',
	},
	{
		code: 'companyBankingNotExist',
		message: 'Số tài khoản không tồn tại !',
	},
];

// ===//==============================//===//
// ${label} or ${name} - Get the name or label of current input
// When you custom label of FormItem, the value of label will be 'undefined'
// so you must add message property to the rule
// ===//==============================//===//
// === Descriptions about Antd ValidateMessages ===//
// === Link: https://github.com/react-component/field-form/blob/master/src/utils/messages.ts
/* eslint-disable */
export const validateMessages: IValidateMessage = {
	required: "'${label}' không được bỏ trống!",
	whitespace: "'${label}' không được bỏ trống!",
	string: {
		len: "'${label}' phải có đúng ${len} kí tự!",
		min: "'${label}' phải có tối thiểu ${min} kí tự!",
		max: "'${label}' không thể dài hơn ${max} kí tự!",
		range: "'${label}' phải trong khoản từ ${min} đến ${max} kí tự!",
	},
	date: {
		format: "'${label}' định dạng thời gian không hợp lệ!",
		parse: "'${label}' không thể chuyển sang định dạng ngày!",
		invalid: "'${label}' thời gian không hợp lệ!",
	},
	number: {
		len: "'${label}' phải bằng ${len}!",
		min: "'${label}' không thể nhỏ hơn ${min}!",
		max: "'${label}' khổng thể lớn hơn ${max}!",
		range: "'${label}' phải ở khoản từ ${min} đến ${max}!",
	},
	array: {
		len: "'${name}' phải có đúng ${len} về độ dài của mảng!",
		min: "'${name}' không được bé hơn ${min} về độ dài của mảng!",
		max: "'${name}' không được lớn hơn ${max} về độ dài của mảng!",
		range: "'${name}' phải trong khoản từ ${min} đến ${max} về độ dài của mảng!",
	},
};
